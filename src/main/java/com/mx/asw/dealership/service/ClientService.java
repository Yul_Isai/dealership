package com.mx.asw.dealership.service;

import com.mx.asw.dealership.commons.model.dto.ClientDTO;
import com.mx.asw.dealership.repository.BaseService;

public interface ClientService extends BaseService<ClientDTO> {
}
