package com.mx.asw.dealership.service.impl;


import com.mx.asw.dealership.commons.exception.ServiceException;
import com.mx.asw.dealership.commons.mapper.CarMapper;
import com.mx.asw.dealership.commons.mapper.ClientMapper;
import com.mx.asw.dealership.commons.model.dto.CarDTO;
import com.mx.asw.dealership.commons.model.entity.CarDO;
import com.mx.asw.dealership.commons.model.entity.ClientDO;
import com.mx.asw.dealership.commons.model.response.MetadataDTO;
import com.mx.asw.dealership.commons.model.response.PaginationDTO;
import com.mx.asw.dealership.commons.model.response.ResponseListDTO;
import com.mx.asw.dealership.commons.model.response.SortDTO;
import com.mx.asw.dealership.repository.CarRepository;
import com.mx.asw.dealership.repository.custom.CarCustomRepository;
import com.mx.asw.dealership.service.CarService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

@Service
public class CarImplService implements CarService {

    private static final Logger logger = Logger.getLogger(CarImplService.class);

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private CarCustomRepository carCustomRepository;

    @Override
    public CarDTO getById(int id) throws ServiceException {

        CarDTO carDTO = null;

        try {
            logger.info("Querying car by id: " + id);
            Optional<CarDO> carDO = carRepository.findByIdCar(id);

            if (!carDO.isPresent()) {
                logger.info("DB OBJECT IS NULL");
                throw new ServiceException("The car doesn't exists");
            }
            carDTO = CarMapper.toGet(carDO.get());
            carDTO.setIdClient(ClientMapper.toGet(carDO.get().getIdClient()));
        } catch (ServiceException e) {
            logger.error("Exception: ", e);
            throw e;
        } catch (Exception e) {
            logger.error("Generic Exception: ", e);
            throw new ServiceException("Generic Error");
        }
        return carDTO;
    }

    @Override
    public ResponseListDTO getByFilter(Map<String, String> filterParams) throws ServiceException {
        ResponseListDTO responseListDTO = new ResponseListDTO();

        try {
            int page = (filterParams.get("page") != null && !filterParams.get("page").equals("")) ? Integer.valueOf("page") : 0;
            int size = (filterParams.get("size") != null && !filterParams.get("size").equals("")) ? Integer.valueOf("size") : 15;
            Pageable pageable = PageRequest.of(page, size, Sort.by(Direction.ASC, "idCar"));

            Specification<CarDO> specification = this.carCustomRepository.queryWhereConditionFilter(filterParams);
            Page<CarDO> carDOS = this.carRepository.findAll(specification, pageable);
            responseListDTO.setData(new ArrayList<CarDTO>());

            if (carDOS != null && !carDOS.getContent().isEmpty()) {
                SortDTO sortDTO = new SortDTO(Direction.ASC.name(), "idCar");
                PaginationDTO paginationDTO = new PaginationDTO(page, size, carDOS.getTotalElements());
                responseListDTO.setData(CarMapper.toGet(carDOS.getContent()));
                responseListDTO.setMetadata(new MetadataDTO(paginationDTO, sortDTO));
            }
        } catch (Exception e) {
            logger.info("ERROR IN FILTERS: ", e);
            throw new ServiceException("Has been occurred an error");
        }
        return responseListDTO;
    }

    @Override
    public CarDTO create(CarDTO carDTO) throws ServiceException {
        logger.info("CREATE NEW CAR: " + carDTO);
        try {
            CarDO carDO = CarMapper.toCreate(carDTO);

            ClientDO clientDO = new ClientDO();
            clientDO.setIdClient(carDTO.getIdClient().getId());
            logger.info("ID: " + carDO.getIdClient());
            carDO.setIdClient(clientDO);

            carRepository.save(carDO);
            carDTO.setId(carDO.getIdCar());

            logger.info("OBJECT: [" + carDTO + "]");
        } catch (Exception e) {
            logger.error("Exception: ", e);
            throw new ServiceException("Generic Error");
        }
        return carDTO;
    }

    @Override
    public CarDTO update(int id, CarDTO carDTO) throws ServiceException {
        logger.info("UPDATE CAR: " + carDTO + "by ID : [" + id + "]");
        try {
            Optional<CarDO> carDO = carRepository.findByIdCar(id);
            if (!carDO.isPresent()) {
                logger.info("THE OBJECT CAR IS NULL");
                throw new ServiceException("The car doesn't exists");
            }
            CarMapper.toUpdate(carDTO, carDO.get());
            carRepository.save(carDO.get());
        } catch (ServiceException e) {
            logger.info("Service Exception", e);
            throw e;
        } catch (Exception e) {
            logger.error("Exception: ", e);
            throw new ServiceException("Generic Error");
        }
        return carDTO;
    }

    @Override
    public void delete(int id) throws ServiceException {
        logger.info("Delete car by ID: " + id);
        try {
            Optional<CarDO> carDO = carRepository.findByIdCar(id);

            if (!carDO.isPresent()) {
                logger.info("THE OBJECT CAR IS NULL");
                throw new ServiceException("The car doesn't exists");
            }
            carRepository.delete(carDO.get());
        } catch (ServiceException e) {
            logger.error("ServiceException" + e);
            throw new ServiceException("Generic Error");
        }
    }
}
