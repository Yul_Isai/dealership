package com.mx.asw.dealership.service.impl;

import com.mx.asw.dealership.commons.exception.ServiceException;
import com.mx.asw.dealership.commons.mapper.ClientMapper;
import com.mx.asw.dealership.commons.model.dto.ClientDTO;
import com.mx.asw.dealership.commons.model.entity.ClientDO;
import com.mx.asw.dealership.commons.model.response.MetadataDTO;
import com.mx.asw.dealership.commons.model.response.PaginationDTO;
import com.mx.asw.dealership.commons.model.response.ResponseListDTO;
import com.mx.asw.dealership.commons.model.response.SortDTO;
import com.mx.asw.dealership.repository.ClientRepository;
import com.mx.asw.dealership.repository.custom.ClientCustomRepository;
import com.mx.asw.dealership.service.ClientService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;


@Service
public class ClientImplService implements ClientService {

    private static final Logger logger = Logger.getLogger(ClientImplService.class);

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientCustomRepository clientCustomRepository;

    @Override
    public ClientDTO getById(int id) throws ServiceException {
        ClientDTO clientDTO = null;

        try {
            logger.info("Querying client by id: " + id);
            Optional<ClientDO> clientDO = clientRepository.findByIdClient(id);

            if (!clientDO.isPresent()) {
                logger.info("DB OBJECT IS NULL");
                throw new ServiceException("The client doesn't exists");
            }
            clientDTO = ClientMapper.toGet(clientDO.get());
        } catch (ServiceException e) {
            logger.error("Service Exception", e);
            throw e;
        } catch (Exception e) {
            logger.error("Generic Exception");
            throw new ServiceException("Generic Error");
        }

        return clientDTO;
    }

    @Override
    public ResponseListDTO getByFilter(Map<String, String> filterParams) throws ServiceException {
        ResponseListDTO responseListDTO = new ResponseListDTO();

        try {
            int page = (filterParams.get("page") != null && !filterParams.get("page").equals("")) ? Integer.valueOf(filterParams.get("page")) : 0;
            int size = (filterParams.get("size") != null && !filterParams.get("size").equals("")) ? Integer.valueOf(filterParams.get("size")) : 15;
            Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "idClient"));

            Specification<ClientDO> specification = this.clientCustomRepository.queryWhereConditionFilter(filterParams);
            Page<ClientDO> clientDOs = this.clientRepository.findAll(specification, pageable);
            responseListDTO.setData(new ArrayList<ClientDTO>());

            if (clientDOs != null && !clientDOs.getContent().isEmpty()) {
                SortDTO sortDTO = new SortDTO(Sort.Direction.ASC.name(), "idClient");
                PaginationDTO paginationDTO = new PaginationDTO(page, size, clientDOs.getTotalElements());
                responseListDTO.setData(ClientMapper.toGet(clientDOs.getContent()));
                responseListDTO.setMetadata(new MetadataDTO(paginationDTO, sortDTO));
            }
        } catch (Exception e) {
            logger.info("Error in filters: ", e);
            throw new ServiceException("Has been occurred an error");
        }

        return responseListDTO;
    }

    @Override
    public ClientDTO create(ClientDTO clientDTO) throws ServiceException {
        logger.info("CREATE A NEW CLIENT: " + clientDTO);
        try {
            ClientDO clientDO = ClientMapper.toCreate(clientDTO);
            clientRepository.save(clientDO);
            clientDTO.setId(clientDO.getIdClient());
            logger.info("OBJECT: [" + clientDTO + "]");
        } catch (Exception e) {
            logger.error("Exception: ", e);
            throw new ServiceException("Generic Error");
        }
        return clientDTO;
    }

    @Override
    public ClientDTO update(int id, ClientDTO baseDTO) throws ServiceException {
        return null;
    }

    @Override
    public void delete(int id) throws ServiceException {

    }
}
