package com.mx.asw.dealership.service;

import com.mx.asw.dealership.commons.model.dto.CarDTO;
import com.mx.asw.dealership.repository.BaseService;

public interface CarService extends BaseService<CarDTO> {
}
