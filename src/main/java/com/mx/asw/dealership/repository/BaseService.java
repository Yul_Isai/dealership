package com.mx.asw.dealership.repository;

import com.mx.asw.dealership.commons.model.response.BaseDTO;
import com.mx.asw.dealership.commons.model.response.ResponseListDTO;
import com.mx.asw.dealership.commons.exception.ServiceException;
import java.util.Map;

public interface BaseService<T extends BaseDTO> {

    public T getById(int id) throws ServiceException;

    ResponseListDTO getByFilter(Map<String, String> filterParams) throws ServiceException;

    T create(T baseDTO) throws ServiceException;

    T update(int id, T baseDTO) throws ServiceException;

    void delete(int id) throws ServiceException;
}
