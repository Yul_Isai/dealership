package com.mx.asw.dealership.repository.custom;

import com.mx.asw.dealership.commons.Utils;
import com.mx.asw.dealership.commons.constants.Constants;
import com.mx.asw.dealership.commons.exception.ServiceException;
import com.mx.asw.dealership.commons.model.entity.CarDO;
import com.mx.asw.dealership.commons.model.entity.ClientDO;
import org.apache.log4j.Logger;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Repository
public class ClientCustomRepository {

    private static final Logger logger = Logger.getLogger(CarCustomRepository.class);
    /**
     * Query dinamico para consulta de clientes
     * @param filter
     * @return
     * @throws ServiceException
     */
    public Specification<ClientDO> queryWhereConditionFilter(Map<String, String> filter) throws ServiceException {
        return (root, query, cb) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.get("name") != null) {
                logger.info("Binding name: " + filter.get("name"));
                predicates.add(cb.and(cb.like(root.get("name"), Utils.builderString("%", filter.get("name"), "%"))));
            }

            if (filter.get("SurnameF") != null) {
                logger.info("Binding name: " + filter.get("name"));
                predicates.add(cb.and(cb.like(root.get("name"), Utils.builderString("%", filter.get("name"), "%"))));
            }

            if(filter .get("surnameM") != null) {
                logger.info("Binding name: " + filter.get("name"));
                predicates.add(cb.and(cb.like(root.get("surnameM"), Utils.builderString("%", filter.get("surnameM"), "%"))));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
