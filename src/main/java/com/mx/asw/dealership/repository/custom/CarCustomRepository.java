package com.mx.asw.dealership.repository.custom;

import com.mx.asw.dealership.commons.Utils;
import com.mx.asw.dealership.commons.exception.ServiceException;
import com.mx.asw.dealership.commons.model.entity.CarDO;
import org.apache.log4j.Logger;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Repository
public class CarCustomRepository {

    private static final Logger logger = Logger.getLogger(CarCustomRepository.class);
    /**
     * Query dinamico para consulta de autos
     * @param filter
     * @return
     * @throws ServiceException
     */
    public Specification<CarDO> queryWhereConditionFilter(Map<String, String> filter) throws ServiceException {
        return (root, query, cb) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.get("registration") != null) {
                logger.info("Binding name: " + filter.get("registration"));
                predicates.add(cb.and(cb.like(root.get("registration"), Utils.builderString("%", filter.get("registration"), "%"))));
            }
            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
