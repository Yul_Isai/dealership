package com.mx.asw.dealership.repository;

import com.mx.asw.dealership.commons.model.entity.ClientDO;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ClientRepository extends CrudRepository<ClientDO, Integer>, JpaSpecificationExecutor<ClientDO> {

    Optional<ClientDO> findByIdClient(int id);
}
