package com.mx.asw.dealership.repository;

import com.mx.asw.dealership.commons.model.entity.CarDO;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CarRepository extends CrudRepository<CarDO, Integer>, JpaSpecificationExecutor<CarDO> {

    Optional<CarDO> findByIdCar(int id);
}
