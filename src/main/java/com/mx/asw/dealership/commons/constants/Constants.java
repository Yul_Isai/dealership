package com.mx.asw.dealership.commons.constants;

public interface Constants {

    /**
     * Cadena vacia para validacioines
     */
    public static final String STRNG_EMPTY = "";

    /**
     * Identificador para el tipo de usuario cliente
     */
    public static final int ID_TYPE_USER_CUSTOMER = 1;

    /**
     * Identificador para el estado del usuario registrado
     */
    public static final int ID_STATUS_USER_REGISTRED = 1;

    /**
     * Identificador para el estado del usuario eliminado
     */
    public static final int ID_STATUS_USER_DELETED = 2;

    /**
     * Identificador para el estado del usuario certificado
     */
    public static final int ID_STATUS_USER_CETIFICATE = 3;

    /**
     * Identificador para el estado del usuario activo
     */
    public static final int ID_STATUS_USER_ACTIVE = 4;

    /**
     * Identificador para el estado del auto activo
     */

    public static final int ID_STATUS_CAR_ACTIVE = 4;

    /**
     * Identificador para el estado del usuario inactivo
     */
    public static final int ID_STATUS_USER_INACTIVE = 5;

    public static final int ID_STATUS_CAR_DELETED = 2;
}
