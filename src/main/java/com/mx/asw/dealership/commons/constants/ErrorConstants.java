package com.mx.asw.dealership.commons.constants;

public interface ErrorConstants {
    /*
     * Prefijo de error
     */
    public static final String PREFIX = "error.message.";

    /**
     * C&oacute;digo gen&eacute;rico de error
     */
    public static final int GENERIC = 2000;
}
