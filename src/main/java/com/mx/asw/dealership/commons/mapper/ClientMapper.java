package com.mx.asw.dealership.commons.mapper;

import com.mx.asw.dealership.commons.model.dto.ClientDTO;
import com.mx.asw.dealership.commons.model.entity.ClientDO;

import java.util.ArrayList;
import java.util.List;

public class ClientMapper {

    private ClientMapper() {
    }

    public static ClientDTO toGet(ClientDO clientDO) {
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setId(clientDO.getIdClient());
        clientDTO.setName(clientDO.getName());
        clientDTO.setSurnameF(clientDO.getSurnameF());
        clientDTO.setSurnameM(clientDO.getSurnameF());
        return clientDTO;
    }

    public static List<ClientDTO> toGet(List<ClientDO> clientDOList) {
        List<ClientDTO> clientDTOS = new ArrayList<>();
        for (ClientDO clientDO : clientDOList) {
            clientDTOS.add(toGet(clientDO));
        }
        return clientDTOS;
    }

    public static ClientDO toCreate(ClientDTO clientDTO) {
        ClientDO clientDO = new ClientDO();
        clientDO.setIdClient(clientDTO.getId());
        clientDO.setName(clientDTO.getName());
        clientDO.setSurnameF(clientDTO.getSurnameF());
        clientDO.setSurnameM(clientDTO.getSurnameM());
        return clientDO;
    }

    public static void toUpdate(ClientDTO clientDTO, ClientDO clientDO) {
        clientDO.setName(clientDTO.getName());
        clientDO.setSurnameF(clientDTO.getSurnameF());
        clientDO.setSurnameM(clientDTO.getSurnameM());
    }
}
