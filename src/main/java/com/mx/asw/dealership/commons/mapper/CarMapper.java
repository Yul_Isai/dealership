package com.mx.asw.dealership.commons.mapper;

import com.mx.asw.dealership.commons.model.dto.CarDTO;
import com.mx.asw.dealership.commons.model.dto.ClientDTO;
import com.mx.asw.dealership.commons.model.entity.CarDO;
import com.mx.asw.dealership.commons.model.entity.ClientDO;

import java.util.ArrayList;
import java.util.List;

public class CarMapper {

    private CarMapper() {
    }

    public static CarDTO toGet(CarDO carDO) {
        CarDTO carDTO = new CarDTO();
        ClientDO clientDO = new ClientDO();
        carDTO.setId(carDO.getIdCar());
        carDTO.setRegistration(carDO.getRegistration());
        return carDTO;
    }

    public static List<CarDTO> toGet(List<CarDO> carDOList) {
        List<CarDTO> carDTOS = new ArrayList<>();
        for (CarDO carDO : carDOList) {
            carDTOS.add(toGet(carDO));
        }
        return carDTOS;
    }

    public static CarDO toCreate(CarDTO carDTO) {
        CarDO carDO = new CarDO();
        carDO.setIdCar(carDTO.getId());
        carDO.setRegistration(carDTO.getRegistration());
        return carDO;
    }

    public static void toUpdate(CarDTO carDTO, CarDO carDO) {
        carDO.setRegistration(carDTO.getRegistration());
    }

}
