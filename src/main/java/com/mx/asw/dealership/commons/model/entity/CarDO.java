package com.mx.asw.dealership.commons.model.entity;

import javax.persistence.*;

@Table(name = "automovil")
@Entity
public class CarDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idauto")
    private Integer idCar;

    @Column(name = "placas")
    private String registration;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idcli")
    private ClientDO idClient;

    public Integer getIdCar() {
        return idCar;
    }

    public void setIdCar(Integer idCar) {
        this.idCar = idCar;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public ClientDO getIdClient() {
        return idClient;
    }

    public void setIdClient(ClientDO idClient) {
        this.idClient = idClient;
    }
}
