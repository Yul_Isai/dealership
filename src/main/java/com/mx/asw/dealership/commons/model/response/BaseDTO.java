package com.mx.asw.dealership.commons.model.response;

import java.io.Serializable;

public class BaseDTO implements Serializable {

    private static final long SerialVersionUID = 1L;

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
