package com.mx.asw.dealership.commons.model.dto;

import com.mx.asw.dealership.commons.model.response.BaseDTO;

public class ClientDTO extends BaseDTO {

    private static final long SerialVersionUID = 1L;
    private String name;
    private String surnameF;
    private String surnameM;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurnameF() {
        return surnameF;
    }

    public void setSurnameF(String surnameF) {
        this.surnameF = surnameF;
    }

    public String getSurnameM() {
        return surnameM;
    }

    public void setSurnameM(String surnameM) {
        this.surnameM = surnameM;
    }

}
