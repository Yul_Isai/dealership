package com.mx.asw.dealership.commons.model.entity;


import javax.persistence.*;

@Table(name = "cliente")
@Entity
public class ClientDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcli")
    private Integer idClient;

    @Column(name = "nom")
    private String name;

    @Column(name = "apepat")
    private String surnameF;

    @Column(name = "apemat")
    private String surnameM;

    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurnameF() {
        return surnameF;
    }

    public void setSurnameF(String surnameF) {
        this.surnameF = surnameF;
    }

    public String getSurnameM() {
        return surnameM;
    }

    public void setSurnameM(String surnameM) {
        this.surnameM = surnameM;
    }
}
