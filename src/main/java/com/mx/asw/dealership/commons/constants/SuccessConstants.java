package com.mx.asw.dealership.commons.constants;

public interface SuccessConstants {

    /**
     * Prefijo de &eacute;xito
     */

    public static final String PREFIX = "success.message.";

    /**
     * C&oacute;digo g&eacute;rico de &eacute;xito
     */
    public static final int GENERIC = 1000;
}
