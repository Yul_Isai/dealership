package com.mx.asw.dealership.commons.model.dto;

import com.mx.asw.dealership.commons.model.entity.ClientDO;
import com.mx.asw.dealership.commons.model.response.BaseDTO;

public class CarDTO extends BaseDTO {

    private String registration;

    private ClientDTO client;

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public ClientDTO getIdClient() {
        return client;
    }

    public void setIdClient(ClientDTO client) {
        this.client = client;
    }
}
