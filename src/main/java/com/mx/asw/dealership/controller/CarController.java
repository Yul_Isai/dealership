package com.mx.asw.dealership.controller;

import com.mx.asw.dealership.commons.constants.ErrorConstants;
import com.mx.asw.dealership.commons.constants.SuccessConstants;
import com.mx.asw.dealership.commons.exception.ServiceException;
import com.mx.asw.dealership.commons.model.dto.CarDTO;
import com.mx.asw.dealership.commons.model.response.BaseResponseDTO;
import com.mx.asw.dealership.commons.model.response.ResponseListDTO;
import com.mx.asw.dealership.commons.model.response.ResponseObjectDTO;
import com.mx.asw.dealership.service.impl.CarImplService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * Controller for car entity
 * the route to consume API is "/api/v1/car"
 *
 * @author: Yul Villegas
 */
@RestController
@RequestMapping("/api/v1/cars")
public class CarController {

    private static final Logger logger = Logger.getLogger(CarController.class);

    @Autowired
    private CarImplService carImplService;

    @Autowired
    private MessageSource messageSource;

    /**
     * Exposes the car query service by id
     *
     * @return Response with detail of car
     * @Param id => Car Identifier
     */
    @ResponseBody
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponseDTO> getById(@PathVariable("id") int id) {
        ResponseObjectDTO responseObjectDTO = new ResponseObjectDTO();

        try {
            String message = this.messageSource.getMessage(SuccessConstants.PREFIX + SuccessConstants.GENERIC, null, LocaleContextHolder.getLocale());
            CarDTO carDTO = this.carImplService.getById(id);

            responseObjectDTO.setData(carDTO);
            responseObjectDTO.setCode(SuccessConstants.GENERIC);
            responseObjectDTO.setMessage(message);
        } catch (ServiceException se) {
            logger.error("", se);
            String message = this.messageSource.getMessage(ErrorConstants.PREFIX + ErrorConstants.GENERIC, null, LocaleContextHolder.getLocale());
            responseObjectDTO.setCode(ErrorConstants.GENERIC);
            responseObjectDTO.setMessage(message);
        } catch (Exception e) {
            logger.error("", e);
            String message = this.messageSource.getMessage(ErrorConstants.PREFIX + ErrorConstants.GENERIC, null, LocaleContextHolder.getLocale());
            responseObjectDTO.setCode(ErrorConstants.GENERIC);
            responseObjectDTO.setMessage(message);
        }
        return new ResponseEntity<BaseResponseDTO>(responseObjectDTO, HttpStatus.OK);
    }

    /**
     * Exposes the car query service by search params
     *
     * @param allParams
     * @return listDTO
     */
    @ResponseBody
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponseDTO> getByFilter(@RequestParam Map<String, String> allParams) {
        ResponseListDTO listDTO = null;
        try {
            logger.info("Filtrar por [" + allParams.entrySet() + "]");

            String message = this.messageSource.getMessage(SuccessConstants.PREFIX + SuccessConstants.GENERIC, null, LocaleContextHolder.getLocale());
            listDTO = this.carImplService.getByFilter(allParams);
            listDTO.setCode(SuccessConstants.GENERIC);
            listDTO.setMessage(message);

        } catch (ServiceException se) {
            logger.error("", se);
            String message = this.messageSource.getMessage(ErrorConstants.PREFIX + ErrorConstants.GENERIC, null, LocaleContextHolder.getLocale());
            listDTO = new ResponseListDTO();
            listDTO.setCode(ErrorConstants.GENERIC);
            listDTO.setMessage(message);
        } catch (Exception e) {
            logger.error("", e);
            String message = this.messageSource.getMessage(ErrorConstants.PREFIX + ErrorConstants.GENERIC, null, LocaleContextHolder.getLocale());
            listDTO = new ResponseListDTO();
            listDTO.setCode(ErrorConstants.GENERIC);
            listDTO.setMessage(message);
        }
        return new ResponseEntity<BaseResponseDTO>(listDTO, HttpStatus.OK);
    }

    /**
     * Exposes the car create service
     * @param carDTO
     * @return responseObjectDTO
     */

    @ResponseBody
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponseDTO> create(@RequestBody CarDTO carDTO) {
        ResponseObjectDTO responseObjectDTO = new ResponseObjectDTO();
        try {
            String message = this.messageSource.getMessage(SuccessConstants.PREFIX + SuccessConstants.GENERIC, null, LocaleContextHolder.getLocale());
            carDTO = this.carImplService.create(carDTO);

            responseObjectDTO.setData(carDTO);
            logger.info("OBJTECT DATA:" + carDTO);
            responseObjectDTO.setCode(SuccessConstants.GENERIC);
            responseObjectDTO.setMessage(message);
        } catch (ServiceException se) {
            String message = this.messageSource.getMessage(ErrorConstants.PREFIX + ErrorConstants.GENERIC, null, LocaleContextHolder.getLocale());
            responseObjectDTO.setCode(ErrorConstants.GENERIC);
            responseObjectDTO.setMessage(message);
        } catch (Exception e) {
            logger.error("", e);
            String message = this.messageSource.getMessage(ErrorConstants.PREFIX + ErrorConstants.GENERIC, null, LocaleContextHolder.getLocale());
            responseObjectDTO.setCode(ErrorConstants.GENERIC);
            responseObjectDTO.setMessage(message);
        }
        return new ResponseEntity<BaseResponseDTO>(responseObjectDTO, HttpStatus.OK);
    }

    /**
     * Exposes the car update service
     * @param id
     * @param carDTO
     * @return responseObjectDTO
     */

    @ResponseBody
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponseDTO> update(@PathVariable("id") int id, @RequestBody CarDTO carDTO) {
        ResponseObjectDTO responseObjectDTO = new ResponseObjectDTO();
        try {
            String message = messageSource.getMessage(SuccessConstants.PREFIX + SuccessConstants.GENERIC, null, LocaleContextHolder.getLocale());
            carDTO = this.carImplService.update(id, carDTO);

            responseObjectDTO.setData(carDTO);
            responseObjectDTO.setCode(SuccessConstants.GENERIC);
            responseObjectDTO.setMessage(message);
        } catch (ServiceException e) {
            logger.error("", e);
            String message = messageSource.getMessage(ErrorConstants.PREFIX + ErrorConstants.GENERIC, null, LocaleContextHolder.getLocale());
            responseObjectDTO.setCode(ErrorConstants.GENERIC);
            responseObjectDTO.setMessage(message);
        } catch (Exception e) {
            logger.error("", e);
            String message = messageSource.getMessage(ErrorConstants.PREFIX + ErrorConstants.GENERIC, null, LocaleContextHolder.getLocale());
            responseObjectDTO.setCode(ErrorConstants.GENERIC);
            responseObjectDTO.setMessage(message);
        }
        return new ResponseEntity<BaseResponseDTO>(responseObjectDTO, HttpStatus.OK);
    }


    /**
     * Exposes the car delete service
     * @param id
     * @return baseResponseDTO
     */
    @ResponseBody
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponseDTO> delete(@PathVariable("id") int id) {
        BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
        try {
            String message = messageSource.getMessage(SuccessConstants.PREFIX + SuccessConstants.GENERIC, null, LocaleContextHolder.getLocale());
            this.carImplService.delete(id);
            baseResponseDTO.setCode(SuccessConstants.GENERIC);
            baseResponseDTO.setMessage(message);
        } catch (ServiceException e) {
            logger.error("Error in delete controller", e);
            String message = messageSource.getMessage(ErrorConstants.PREFIX + ErrorConstants.GENERIC, null, LocaleContextHolder.getLocale());
            baseResponseDTO.setCode(ErrorConstants.GENERIC);
            baseResponseDTO.setMessage(message);
        } catch (Exception e) {
            logger.error("", e);
            String message = messageSource.getMessage(ErrorConstants.PREFIX + ErrorConstants.GENERIC, null, LocaleContextHolder.getLocale());
            baseResponseDTO.setCode(ErrorConstants.GENERIC);
            baseResponseDTO.setMessage(message);
        }
        return new ResponseEntity<BaseResponseDTO>(baseResponseDTO, HttpStatus.OK);
    }
}
